const supertest = require('supertest');
const express = require('express');
const rawBodyParser = require('../../lib/raw-body-parser');
const getAvailablePort = require('../util/get-available-port');
const makeSafeExpectCallback = require('../util/make-safe-expect-callback')

const port = getAvailablePort();
const config = require('../../lib/config');
config.set('targetEndpoint', `http://localhost:${port}/wd/hub`);

const gitlabSeleniumServerApp = require('../../server/app');
const gitlabSeleniumServerAgent = supertest.agent(gitlabSeleniumServerApp);

describe('Basic proxy behavior test', () => {
  let targetTestApp;
  let targetTestServer;
  beforeEach((done) => {
    targetTestApp = express();
    targetTestApp.use(rawBodyParser);
    targetTestServer = targetTestApp.listen(port, done);
  });

  afterEach(() => {
    targetTestServer.close();
  });


  it('should send root is up response', (done) => {
    gitlabSeleniumServerAgent
      .get('/')
      .expect(200, done);
  });

  it('should forward on a GET request that returns application/json', (done) => {
    const endpointToHit = '/wd/hub/session/123/log/types';
    const expectedResponse = { foo: 'bar' };

    targetTestApp.get('*', makeSafeExpectCallback(done, (req, res) => {
      expect(req.path).toBe(endpointToHit);
      res.status(200).json(expectedResponse);
    }));

    gitlabSeleniumServerAgent
      .get(endpointToHit)
      .expect('Content-Type', /json/)
      .expect(200, expectedResponse, done);
  });

  it('should forward on a POST request that returns application/json', (done) => {
    const endpointToHit = '/wd/hub/session/123/something';
    const expectedResponse = { foo: 'bar' };

    targetTestApp.post('*', makeSafeExpectCallback(done, (req, res) => {
      expect(req.path).toBe(endpointToHit);
      res.status(200).json(expectedResponse);
    }));

    gitlabSeleniumServerAgent
      .post(endpointToHit)
      .expect('Content-Type', /json/)
      .expect(200, expectedResponse, done);
  });

  it('should forward on a POST request with body', (done) => {
    const endpointToHit = '/wd/hub/session/123/something';
    const postBody = `{ foo: 'bar' }`;

    targetTestApp.post('*', makeSafeExpectCallback(done, (req, res) => {
      expect(String(req.body)).toBe(postBody);
      res.status(200);
    }));

    gitlabSeleniumServerAgent
      .post(endpointToHit)
      .set('Content-Type', 'application/json')
      .send(postBody)
      .expect(200, done);
  });

  it('should forward on a DELETE request that returns application/json', (done) => {
    const endpointToHit = '/wd/hub/session/123/something';

    targetTestApp.post('*', makeSafeExpectCallback(done, (req, res) => {
      expect(req.path).toBe(endpointToHit);
      res.status(200).json();
    }));

    gitlabSeleniumServerAgent
      .post(endpointToHit)
      .expect('Content-Type', /json/)
      .expect(200, '', done);
  });

})
