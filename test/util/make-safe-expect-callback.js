// via https://github.com/facebook/jest/issues/3519#issuecomment-309131526
function makeSafeExpectCallback(done, actualCb) {
  return (...args) => {
    try {
      actualCb(...args);
      done();
    } catch (error) {
      done.fail(error);
    }
  };
}

module.exports = makeSafeExpectCallback;
